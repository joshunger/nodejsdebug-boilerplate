const koa = require('koa');
const app = koa();

const server = app.listen(() => {
  console.log('opened server on', server.address().port);
});
